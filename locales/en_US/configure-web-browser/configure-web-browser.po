msgid ""
msgstr ""
"Project-Id-Version: f123-files\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-04 14:21-0400\n"
"PO-Revision-Date: 2019-09-04 19:24\n"
"Last-Translator: Fernando Botelho (FernandoBotelho)\n"
"Language-Team: English\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: f123-files\n"
"X-Crowdin-Language: en\n"
"X-Crowdin-File: /master/po/configure-web-browser/configure-web-browser.pot\n"

#: configure-web-browser.sh:53
msgid "Firefox: the mozilla Firefox web browser"
msgstr ""

#: configure-web-browser.sh:54
msgid "Seamonkey: the Mozilla internet suite of internet applications."
msgstr ""

#: configure-web-browser.sh:72
msgid "Please wait while the following application is installed:"
msgstr ""

#: configure-web-browser.sh:73
msgid "There was a problem installing The requested browser. Please try again later."
msgstr ""

#: configure-web-browser.sh:99
msgid "has been set as your default browser."
msgstr ""

