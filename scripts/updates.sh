#!/bin/bash
#
# Copyright 2018, F123 Consulting, <information@f123.org>
# Copyright 2018, Kyle, <kyle@free2.ml>
# Copyright 2018, Storm Dragon <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--

# This is a helper script for update-f123light to perform incremental updates based on a timestamp
# It will not be installed to a running system
#
# Incremental updates are only to be performed if their timestamp is later than
# /etc/timestamp-f123light on the system being updated.
# Each update should be formatted as
#
# update yymmddhhmm && {
# 	do-something here;
# 	do-something-else here;
# }
# If an update requires a reboot, add
# reboot=true
# anywhere in the update to prompt the user to reboot afterward
#
# Any new updates should be placed at the end of this file for best readability
# It is recommended to skip a line between updates, also for readability
# To get the current date and time in the format, use the command
# \date '+%y%m%d%H%M'
# Remember to add a ; after each line of the update group.

# Log writing function
log() {
    while read -r line ; do
        echo "$line" | sudo tee -a "$logFile" &> /dev/null
    done
}
                                                                                                                                                                
logFile="/var/log/incremental-updates"
# Clear previous logs
echo -n | sudo tee "$logFile" &> /dev/null


update 1811072200 && {
    # Install Pianobar Pandora client on builds last updated before 7 November 2018
    sudo pacman -S pianobar-git --noconfirm --needed &> /dev/null;
}

update 1811142036 && {
    sudo pacman -S --noconfirm --needed python-magic-wormhole &> /dev/null;
}

update 1811150406 && {
    # There has been a change to the pacman repository URL.
    # Copy the new pacman.conf file to all systems built earlier than 11 November 2018;
    sudo cp /tmp/files-F123Light/files/etc/pacman.conf /etc;
}

update 1811161939 && {
    # Install the update-f123light package;
    sudo pacman -Sy --noconfirm --needed --overwrite /usr/bin/update-f123light update-f123light &> /dev/null;
}

update 1811191827 && {
    # Blacklist non-working bluetooth module;
    echo "blacklist  btsdio" | sudo tee -a /etc/modules-load.d/bluetooth &> /dev/null;
    # Disable bluetooth service;
    sudo systemctl -q disable bluetooth;
    # Enable new bluetooth service;
    sudo systemctl -q enable brcm43438.service;
    # Add RHVoice module to speech-dispatcher;
    grep -q 'rhvoice.conf' /etc/speech-dispatcher/speechd.conf || sudo sed -i 's/"espeak-mbrola-generic\.conf"/"espeak-mbrola-generic.conf"\n AddModule "rhvoice"    "sd_rhvoice"  "rhvoice.conf"/' /etc/speech-dispatcher/speechd.conf;
    # Create placeholder file for RHVoice;
    echo '# Placeholder for the rhvoice module.' | sudo tee /etc/speech-dispatcher/modules/rhvoice.conf &> /dev/null;
}

update 1811281732 && {
    # Re-enable the bluetooth service;
    sudo systemctl -q enable --now bluetooth;
    # Power on the bluetooth adapter;
    bluetoothctl power on &> /dev/null;
    # Put the bluetooth settings file in place;
    sudo cp /tmp/files-F123Light/files/etc/bluetooth/main.conf /etc/bluetooth/main.conf;
    # configure speech-dispatcher to use pulseaudio.;
    sudo sed -i 's/^\s*AudioOutputMethod.*/ AudioOutputMethod   pulse/' /etc/speech-dispatcher/speechd.conf;
}

update 1901151236 && {
    # We will need to reboot after this completes for all the groups and users stuff to take affect, and to get pulse started.;
    reboot=true;
    # Create users and groups;
    sudo groupadd --system pulse &> /dev/null;
    sudo groupadd --system pulse-access &> /dev/null;
    sudo useradd --system -g pulse -G audio -d /var/run/pulse -m -s /bin/nologin pulse &> /dev/null;
    sudo gpasswd -a pulse lp &> /dev/null;
    sudo gpasswd -a root pulse-access &> /dev/null;
    for u in $(awk -v OFS=' ' -F':' '{ if ( $3 >= 1000 && $3 <= 60000 && $7 != "/sbin/nologin" && $7 != "/bin/false" ) print $1 ; }' /etc/passwd) ; do sudo gpasswd -a $u pulse-access &> /dev/null;done;
    # Install new packages;
    sudo pacman -Sy --noconfirm --needed pulseaudio-bluetooth pulseaudio-alsa &> /dev/null;
    # move configuration files into place;
    sudo cp -r /tmp/files-F123Light/files/etc/pulse/ /etc;
    sudo cp -ru /tmp/files-F123Light/files/etc/dbus-1/ /etc;
    sudo systemctl -q enable pulseaudio;
    echo "default_driver=pulse" | sudo tee /etc/libao.conf &> /dev/null
}


update 1902200926 && {
    # Update nano configuration file;
    sudo cp /tmp/files-F123Light/files/etc/nanorc /etc;
    # Update Fenrir keyboard layouts;
    sudo cp -r /tmp/files-F123Light/files/etc/fenrirscreenreader/keyboard /etc/fenrirscreenreader;
    # Switch from speech-dispatcher-git to the newer stable version 0.9.0;
    # Remove old unneeded packages.;
    # To prevent errors, only remove one package per statement;
    sudo pacman -Rdd --noconfirm brltty &> /dev/null;
    sudo pacman -Rdd --noconfirm lios-git &> /dev/null;
    sudo pacman -Rdd --noconfirm python-espeak &> /dev/null;
    sudo pacman -Rdd --noconfirm speech-dispatcher-git &> /dev/null;
    # Install new packages.;
    sudo pacman -Sy --noconfirm --needed brltty-minimal cfh-client irssi speech-dispatcher &> /dev/null;
    # Copy .pacsave for speech-dispatcher into place.;
    sudo cp /etc/speech-dispatcher/speechd.conf.pacsave /etc/speech-dispatcher/speechd.conf &> /dev/null;
}


update 1903140649 && {
	# Install the horseshoes package.;
	sudo pacman -Sy --needed --noconfirm horseshoes-git |& log;
	# Install tesseract data packages.;
	sudo pacman -S --needed --noconfirm tesseract-data-{ara,eng,fra,por,spa} |& log;
    # Update Fenrir keyboard layouts;
    sudo cp -rv /tmp/files-F123Light/files/etc/fenrirscreenreader/keyboard /etc/fenrirscreenreader |& log;
}


update 1903300948 && {
    # Update nanorc;
    sudo cp /tmp/files-F123Light/files/etc/nanorc /etc;
}


update 1903311817 && {
    # uninstall python-ipaddress and python-magic-wormhole;
    sudo pacman -Rdd --noconfirm python-ipaddress python-magic-wormhole |& log;
    # Install magic-wormhole;
    sudo pacman -Sy --noconfirm --needed magic-wormhole |& log;
}


update 1904030652 && {
    # uninstall mumble-snapshot, youtube-viewer-git and youtube-dl-git;
    sudo pacman -Rdd --noconfirm mumble-snapshot |& log;
    sudo pacman -Rdd --noconfirm youtube-dl-git |& log;
    sudo pacman -Rdd --noconfirm youtube-viewer-git |& log;
    # Install barnard;
    sudo pacman -Sy --noconfirm --needed barnard-git |& log;
    # Install youtube-dl;
    sudo pacman -Sy --noconfirm --needed youtube-dl |& log;
    # Install youtube-viewer;
    sudo pacman -Sy --noconfirm --needed youtube-viewer |& log;
    # Fix commented out bookmark 8 in fenrir keyboard layouts.;
    sudo sed -i 's/^#KEY_FENRIR,KEY_8=bookmark_8/KEY_FENRIR,KEY_8=bookmark_8/' /etc/fenrirscreenreader/keyboard/*.conf;
}


update 1904151249 && {
    # Install nice editor (ne);
    sudo pacman -Sy --noconfirm --needed ne |& log;
    # Install dbus-broker;
    sudo pacman -Sy --noconfirm --needed dbus-broker |& log;
    # configure dbus-broker for all users;
    sudo systemctl enable --now dbus-broker.service |& log;
    sudo systemctl --global enable dbus-broker.service |& log;
}


update 1905021427 && {
    # Keep track of the date so we can set it after systemd breaks it;
    currentDate="$(date)";
    # Install systemd packages;
    sudo pacman -Sy --noconfirm systemd systemd-libs systemd-sysvcompat |& log;
    # Fix the date so DNS doesn't break.;
    sudo date -s "$currentDate";
}


update 1905061331 && {
    # Install Dragonfm-git;
    sudo pacman -Sy --noconfirm --needed dragonfm-git |& log;
}

update 1906122152 && {
    # Make sure timedatectl is sett to sync time.;
    sudo timedatectl set-ntp ttrue;
}

update 1907211627 && {
    # Create dnssec override.;
    sudo cp -rv /tmp/files-F123Light/files/etc/systemd/resolved.conf.d /etc/systemd/ |& log
}

