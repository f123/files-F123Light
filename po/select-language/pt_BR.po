# Portuguese translations for select-language
# Traduções em português brasileiro para o select-language.
# Copyright (C) 2019 F123 Consulting <info@f123.org>
# Copyright (C) 2019 Kyle <kyle@free2.ml>
# Copyright (C) 2019 Cleverson Casarin Uliana <clul@mm.st>
# This file is distributed under the same license as the F123Light build system.
#
msgid ""
msgstr ""
"Project-Id-Version: files-F 123Light\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-31 23:08-0500\n"
"PO-Revision-Date: 2019-01-31 23:09-0500\n"
"Last-Translator: Cleverson Casarin Uliana <clul@mm.st>"
"Language-Team: Brazilian Portuguese\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../files/usr/lib/F123-wrappers/select-language.sh:38
msgid ""
"Please press 'Enter' if you will use F123Light in English, or use the up and "
"down arrow keys to select another language. Press 'Enter' once you have "
"found the language you would like to use most often in this computer."
msgstr ""
"Por favor pressione Enter se for usar o F123Light em Português, ou use as "
"setas cima e baixo para selecionar outro idioma. Pressione Enter quando "
"tiver encontrado o idioma que gostaria de usar com mais freqüência neste "
"computador."

#: ../../files/usr/lib/F123-wrappers/select-language.sh:76
msgid "-e"
msgstr "-e"
