# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-18 09:24-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:32
msgid "Welcome to F123Light"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:35
msgid "F123 Light Main Menu"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:35
msgid ""
"Use the up and down arrow keys to select your choice and press the 'Enter' "
"key to activate it."
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:36
msgid "_Games Menu (G)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:37
msgid "_Internet Menu (I)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:37
msgid "Browser, e-mail and chat applications"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:38
msgid "_Media Menu (M)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:38
msgid "Book reading, music and video applications"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:39
msgid "_Office Menu (O)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:39
msgid "text, calendar and spreadsheet applications"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:40
msgid "_File Manager (F)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:40
msgid "Copy, move and delete files"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:41
msgid "Manage External _Drives (D)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:43
msgid "External"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:43
msgid "Select Drive"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:50
msgid "Safely remove"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:53
msgid "Main Menu"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:57
msgid "Custom Accessories Menu (_U)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:57
#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:334
msgid "User Defined Applications"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:58
msgid "Search"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:59
msgid "Search This _Computer (C)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:60
msgid "Search the _Web (W)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:62
msgid "_Settings Menu (S)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:62
msgid "Configure this computer"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:63
msgid "_Help Menu (H)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:63
#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:317
msgid "Get Help with F123Light"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:64
msgid "_Turn Off or Restart Computer (T)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:66
msgid "E_xit to Command Line"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:69
msgid "Games"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:70
msgid "_Adventure (A)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:71
msgid "_Arithmetic Challenge! (A)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:74
msgid "_Battlestar (B)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:78
msgid "_Go Fish (G)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:79
msgid "_Gomoku (G)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:80
msgid "_Hangman (H)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:81
msgid "Horseshoes (_H)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:91
msgid "Legends of _Kallisti (K)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:92
msgid "_Mille Bornes (M)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:93
msgid "_Number (N)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:94
msgid "_Phantasia (P)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:95
msgid "_Phase of the Moon (P)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:96
msgid "_Primes (P)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:98
msgid "_Sail (S)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:101
msgid "_Trek (T)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:111
msgid "_Tux Type (T)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:121
msgid "_Wumpus (W)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:123
#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:162
#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:227
#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:286
#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:308
#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:331
msgid "_Main Menu (M)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:126
msgid "Internet"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:126
msgid "Internet programs"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:127
msgid "E-_mail (M)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:130
msgid "Web Browsers"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:131
msgid "_Basic Web Browser (W3M) (B)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:136
msgid "_Full Web Browser (Firefox) (F)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:145
msgid "Communication"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:151
msgid "_Text Chat (Pidgin) (T)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:160
msgid "Voice Chat (Mumble) (_V)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:164
msgid "Voice Chat (Mumble)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:165
msgid "Add Server (_A)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:166
msgid "Remove Server (_R)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:167
msgid "Connect (_C)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:174
msgid "Internet Menu (_M)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:176
msgid "Media"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:176
msgid "Multi-media applications"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:187
msgid "Stringed _Instrument Tuner (I)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:212
msgid "Book Readers"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:213
msgid "_Book Reader (B)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:215
msgid "Books"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:215
msgid "Select book to read"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:222
msgid "Media Menu"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:229
msgid "Office"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:229
msgid "Word processing, calendar, etc"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:237
msgid "_Text Editor (T)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:247
msgid "OCR"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:248
msgid "Lios OCR (_L)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:257
msgid "Office Suite"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:289
msgid "Settings"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:289
msgid "System configuration"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:290
msgid "System Backup Menu (_Y)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:291
msgid "Check for System _Updates (U)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:292
msgid "_Change Passwords (C)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:293
msgid "E-_mail Configuration (M)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:294
msgid "Securit_y Configuration (Y)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:295
msgid "Configure fast _language switching (L)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:296
msgid "Change System S_peech (P)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:297
msgid "_Bluetooth manager (B)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:306
msgid "Configure _Wifi (W)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:310
msgid "Backup or restore your computer"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:311
msgid "Backup your data and settings (_B)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:312
msgid "Restore your data and settings (_R)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:313
msgid "Full system backup (_F)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:315
msgid "Settings Menu (_S)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:317
msgid "Get Help with F123 Light"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:318
msgid "_Get Help (G)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:319
msgid "Access text chat to request help (_C)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:320
msgid "Request remote assistance with my computer (_R)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:322
msgid "About F123Light (_A)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:324
msgid "_Main Menu"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:326
msgid "Turn off or Restart Computer"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:326
msgid "Shutdown or restart your computer"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:327
msgid "_Lock (L)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:328
msgid "Turn _Off (O)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:329
msgid "_Restart (R)"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/pdmenu_default.sh:337
msgid "Main Menu (_M)"
msgstr ""

#: files/usr/lib/F123-includes/script_functions.sh:36
#: files/usr/lib/F123-includes/script_functions.sh:40
#: files/usr/lib/F123-includes/script_functions.sh:50
#: files/usr/lib/F123-includes/script_functions.sh:54
msgid "Enter text and press enter."
msgstr ""

#: files/usr/lib/F123-includes/script_functions.sh:64
msgid "F123Light"
msgstr ""

#: files/usr/lib/F123-includes/script_functions.sh:88
#: files/usr/lib/F123-includes/script_functions.sh:94
msgid "Press 'Enter' for \"yes\" or 'Escape' for \"no\"."
msgstr ""

#: files/usr/lib/F123-includes/script_functions.sh:129
#: files/usr/lib/F123-includes/script_functions.sh:134
msgid ""
"Use the up and down arrow keys to find the option you want, then press enter "
"to select it."
msgstr ""

#: files/usr/lib/F123-includes/script_functions.sh:132
#: files/usr/lib/F123-includes/script_functions.sh:134
msgid "Please select one"
msgstr ""

#: files/usr/lib/F123-includes/script_functions.sh:144
msgid "F123Light - Please Wait"
msgstr ""
