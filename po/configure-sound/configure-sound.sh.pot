# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-18 09:24-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: files/usr/lib/F123-wrappers/pt-BR/configure-sound.sh:36
msgid "USB_Soundcard"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/configure-sound.sh:36
msgid "3MM_Jack"
msgstr ""

#: files/usr/lib/F123-wrappers/pt-BR/configure-sound.sh:45
msgid ""
"You need to reboot for the changes to take affect. Would you like to reboot "
"now?"
msgstr ""

#: files/usr/lib/F123-includes/script_functions.sh:36
#: files/usr/lib/F123-includes/script_functions.sh:40
#: files/usr/lib/F123-includes/script_functions.sh:50
#: files/usr/lib/F123-includes/script_functions.sh:54
msgid "Enter text and press enter."
msgstr ""

#: files/usr/lib/F123-includes/script_functions.sh:64
msgid "F123Light"
msgstr ""

#: files/usr/lib/F123-includes/script_functions.sh:88
#: files/usr/lib/F123-includes/script_functions.sh:94
msgid "Press 'Enter' for \"yes\" or 'Escape' for \"no\"."
msgstr ""

#: files/usr/lib/F123-includes/script_functions.sh:129
#: files/usr/lib/F123-includes/script_functions.sh:134
msgid ""
"Use the up and down arrow keys to find the option you want, then press enter "
"to select it."
msgstr ""

#: files/usr/lib/F123-includes/script_functions.sh:132
#: files/usr/lib/F123-includes/script_functions.sh:134
msgid "Please select one"
msgstr ""

#: files/usr/lib/F123-includes/script_functions.sh:144
msgid "F123Light - Please Wait"
msgstr ""
