#!/bin/bash
# switch-language__-__key_l.sh
# Description: Fast language switching in the console
#
# Copyright 2018, F123 Consulting, <information@f123.org>
# Copyright 2018, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--
 

# Get the language index
langIndex="$(tail -1 ${0/key_shift__+__/})"
if ! [[ "$langIndex" =~ ^[0-9]+$ ]]; then
	langIndex=0
fi
# Find the last line of the script.
lastLine="$(grep -n '^exit 0' $0)"
# Convert lastLine to a number andd increment it by 1.
lastLine=${lastLine%%:*}
lastLine=$((lastLine + 1))
# Delete everything after the exit 0
sudo sed -i "${lastLine}"',$d' $0
# Set language options.
if [[ -r /etc/languages ]]; then
	source /etc/languages
else
	languages=(${LANG%%.*})
	echo "languages=(${languages[@]})" | sudo tee /etc/languages &> /dev/null
	chmod 644 /etc/languages
fi

# Handle incrementing or decrementing the language based on the name of the script.
key="${0#*key_}"
key="${key%.sh}"
case "$key" in
	"l") ((langIndex++));;
	"shift__+__key_l") ((langIndex--));;
esac
# Make sure that the index wraps if not within boundaries.
if [[ $langIndex -lt 0 ]]; then
	langIndex=$((${#languages[@]} - 1))
fi
if [[ $langIndex -ge ${#languages[@]} ]]; then
	langIndex=0
fi

# Write the new langIndex to this script.
echo "$langIndex" | sudo tee -a "${0/key_shift__+__/}" &> /dev/null

# All the initial setup is done. Apply the new language.
# Languages/voices are not always the same as the first 2 of the language, so handled in a case statement.
if [[ "${LANG%%.*}" == "${languages[langIndex]}" ]]; then
	for i in /tmp/fenrirscreenreader-*.sock ; do echo "setting reset" | socat - UNIX-CLIENT:$i ; done
else
case "${languages[langIndex]}" in
			"en_US") for i in /tmp/fenrirscreenreader-*.sock ; do echo "setting set speech#language=en;speech#module=rhvoice;general#spellCheckLanguage=${languages[langIndex]}" | socat - UNIX-CLIENT:$i ; done;;
			"pt_BR") for i in /tmp/fenrirscreenreader-*.sock ; do echo "setting set speech#language=pt;speech#voice=Letícia-F123;speech#module=rhvoice;general#spellCheckLanguage=${languages[langIndex]}" | socat - UNIX-CLIENT:$i ; done;;
			*) for i in /tmp/fenrirscreenreader-*.sock ; do echo "setting set speech#language=${languages[langIndex]::2};speech#module=espeak-ng;general#spellCheckLanguage=${languages[langIndex]}" | socat - UNIX-CLIENT:$i ; done;;
	esac
fi
if [[ -f "/usr/share/kbd/keymaps/i386/qwerty/${languages[$langIndex]}.map.gz" ]]; then
	sudo loadkeys ${languages[$langIndex]}
else
	keyMap="$(grep 'KEYMAP=' /etc/vconsole.conf | cut -d '=' -f2)"
	[[ -n "$keyMap" ]] && sudo loadkeys $keyMap
fi
echo "command say ${languages[langIndex]}" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
exit 0
