��          4      L       `     a      s  {  �         &                    Do you want to hear your password spoken out-loud as you type it? This would make entering the password easier for some persons, but it is also a security risk if your computer has a screen where people can read what you write, or if someone is hearing what you are typing. Passwords do not match Project-Id-Version: files-F 123Light
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-01-31 23:05-0500
PO-Revision-Date: 2019-01-31 23:05-0500
Last-Translator: Cleverson Casarin Uliana <clul@mm.st>Language-Team: Brazilian Portuguese
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Deseja ouvir a senha em voz alta conforme a digita? Isso torna mais fácil para algumas pessoas inserir a senha, mas é também um risco de segurança caso seu computador possua uma tela na qual as pessoas possam ler o que você escreve, ou se alguém estiver ouvindo o que você digita. Senhas não batem 