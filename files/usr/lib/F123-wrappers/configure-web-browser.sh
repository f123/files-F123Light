#!/bin/bash
# configure-web-browser.sh
# Description: Help set browser options between available graphical browsers.
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--
 
# Load functions and reusable code:
if [[ -d /usr/lib/F123-includes/ ]]; then
	for i in /usr/lib/F123-includes/*.sh ; do
		source $i
	done
fi

# the gettext essentials
export TEXTDOMAIN=configure-web-browser
export TEXTDOMAINDIR=/usr/share/locale
source gettext.sh

# Log writing function
log() {
    # Usage: command | log for just stdout.
	# Or command |& log for stderr and stdout.
    while read -r line ; do
        echo "$line" | tee -a "$logFile" &> /dev/null
    done
}
 
# Log file name is ~/.cache/scriptname
logFile="~/.cache/${0##*/}"
# Clear previous logs
echo -n | tee "$logFile" &> /dev/null

# array of browsers
declare -A browsers=(
    [firefox]="$(gettext "Firefox: the mozilla Firefox web browser")"
    [seamonkey]="$(gettext "Seamonkey: the Mozilla internet suite of internet applications.")"
    )

set -o pipefail
ifs="$IFS"
IFS=$'\n'
browser=$(menulist "$(for i in "${!browsers[@]}" ; do
    echo "$i"
    echo "${browsers[$i]}"
done)")

# Exit if nothing was selected
if [[ -z "${browser}" ]]; then
    exit 0
fi

# Install missing packages:
if ! command -v "$browser" &> /dev/null ; then
    if ! yay -Sy --noconfirm --needed $browser | dialog --clear --progressbox "$(gettext "Please wait while the following application is installed:") $browser" 0 0 ; then
        infobox "$(gettext "There was a problem installing The requested browser. Please try again later.")"
        exit 1
    fi
fi

# There are several files that need to be updated with the new browser selection.
# Format brrowserList so it can be used in sed:
unset browserList
for i in ${!browsers[@]} ; do
    browserList+="${i}\|"
done
# remove the final unneeded \|
browserList="${browserList::-2}"

# change ~/.bashrc
sed -i "s/export BROWSER=\($browserList\)/export BROWSER=$browser/" ~/.bashrc |& log

# Update ratpoison configuration
sed -i "s/definekey top C-M-w exec .*/definekey top C-M-w exec $browser/" ~/.ratpoisonrc |& log

# Update the external browser in w3m config.
sed -i "s#^extbrowser .*#extbrowser /usr/bin/$browser#" ~/.w3m/config |& log

# Update all mygtkmenu settings files.
sudo sed -i "/cmd = .*-mail/!s/cmd = \($browserList\).*/cmd = $browser/" /etc/mygtkmenu/* |& log

ratpoison -c "source $HOME/.ratpoisonrc" | dialog --clear --programbox "$browser $(gettext "has been set as your default browser.")" 0 0
exit 0
