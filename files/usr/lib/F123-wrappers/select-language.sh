#!/bin/bash
# Select-language
#
# Copyright 2018, F123 Consulting, <information@f123.org>
# Copyright 2018, Storm Dragon, <storm_dragon@linux-a11y.org>
# Copyright 2018, Kyle, <kyle@free2.ml>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--

export TEXTDOMAIN=select-language
export TEXTDOMAINDIR=/usr/share/language
. gettext.sh

# Log writing function
log() {
    while read -r line ; do
        echo "$line" | sudo tee -a "$logFile" &> /dev/null
    done
}
                                                                                                                                                                
# Log file name is /var/log/scriptname
logFile="/var/log/${0##*/}"
# Clear previous logs
echo -n | sudo tee "$logFile" &> /dev/null
                                                                                                                                                                
  
# For additional language options, add them to the for list and add a corresponding line to the case below the menulist
ifs="$IFS"
IFS=$'\n'
for l in "American English" "Brazilian Portuguese" "Castilian Spanish" "Egyptian Arabic" ; do
    # Dialog requires 2 options for the menu, we hide the tags, but it still needs to be sent twice.
    languages+=("$l" "$l")
done

language="$(dialog --visit-items --backtitle " " --no-tags --menu \
    "$(gettext "Please press 'Enter' if you will use F123Light in English, or use the up and down arrow keys to select another language. Press 'Enter' once you have found the language you would like to use most often in this computer.")" \
    0 0 0 ${languages[@]} --stdout)"
IFS="$ifs"

# The case that corresponds to the for loop above
case "$language" in
    'American English') locale=en_US.UTF-8; speechProvider=rhvoice;;
    'Brazilian Portuguese') locale=pt_BR.UTF-8; speechProvider=rhvoice;;
    'Castilian Spanish') locale=es_ES.UTF-8; speechProvider=espeak-ng-mbrola-generic;;
    'Egyptian Arabic') locale=ar_EG.UTF-8; speechProvider=espeak-ng-mbrola-generic;;
esac

# Exit if language remains unset, i.e.
# if the user has pressed the escape key to close the menu
if [[ -z "$language" ]]; then
	echo "No language selected." | log
	exit 0
fi

# Load the correct font.
# Load the correct custom keymap at boot if present
case "${locale%%.*}" in
    "ar_EG") fontName="/usr/share/kbd/consolefonts/iso06.16.gz";;
    *) fontName="lat2-16";;
esac
echo "FONT=${fontName}" | sudo tee "/etc/vconsole.conf" &> /dev/null
if [[ -f "/usr/share/kbd/keymaps/i386/qwerty/${locale%%.*}.map.gz" ]]; then
    echo "KEYMAP=${locale%%.*}" | sudo tee -a "/etc/vconsole.conf" &> /dev/null
elif [[ -f "/usr/share/kbd/keymaps/i386/qwerty/${locale%%_*}.map.gz" ]]; then
    echo "KEYMAP=${locale%%_*}" | sudo tee -a "/etc/vconsole.conf" &> /dev/null
fi
sudo systemctl enable systemd-vconsole-setup.service |& log

# Set the  chosen language for system locale and speech.
sudo sed -i -e "s/LANG=.*/LANG=${locale}/" /etc/locale.conf

# Backup fenrir settings.
sudo cp /etc/fenrirscreenreader/settings/settings.conf /etc/F123-Config/backup/fenrir.conf
sudo cp /etc/fenrirscreenreader/settings/xterm.conf /etc/F123-Config/backup/xterm.conf
case "${locale::5}" in
    "en_US")
        sudo sed -i -e "s/^[#[:space:]]*DefaultLanguage [[:space:]]*\S*$/ DefaultLanguage   en/" -e 's/\(.*Language\) \(.*\)\..*/\1\L\2/' "/etc/speech-dispatcher/speechd.conf" |& log
        sudo sed -i 's/^\(#v\|v\)oice.*/voice=bdl/' /etc/fenrirscreenreader/settings/settings.conf /etc/fenrirscreenreader/settings/xterm.conf |& log
        sed -i 's/"name": ".*,/"name": "bdl",/' ~/.local/share/orca/user-settings.conf;;
        sudo localectl --no-convert set-x11-keymap us "" "" grp:alt_shift_toggle
    "pt_BR")
        sudo sed -i -e "s/^[#[:space:]]*DefaultLanguage [[:space:]]*\S*$/ DefaultLanguage   pt/" -e 's/\(.*Language\) \(.*\)\..*/\1\L\2/' "/etc/speech-dispatcher/speechd.conf" |& log
        sudo sed -i 's/^voice=/#voice=/' /etc/fenrirscreenreader/settings/settings.conf /etc/fenrirscreenreader/settings/xterm.conf |& log
        sudo localectl --no-convert set-x11-keymap br,us "" "" grp:alt_shift_toggle
        sed -i 's/"name": ".*,/"name": "",/' ~/.local/share/orca/user-settings.conf;;
    *)
        sudo sed -i -e "s/^[#[:space:]]*DefaultLanguage [[:space:]]*\S*$/ DefaultLanguage   ${locale::2}/" "/etc/speech-dispatcher/speechd.conf" |& log
        sudo sed -i 's/^voice=/#voice=/' /etc/fenrirscreenreader/settings/settings.conf /etc/fenrirscreenreader/settings/xterm.conf |& log
        sudo localectl --no-convert set-x11-keymap ${locale::2},us "" "" grp:alt_shift_toggle
        sed -i 's/"name": ".*,/"name": "",/' ~/.local/share/orca/user-settings.conf;;
esac
sudo sed -i "s/^[[:space:]]*DefaultModule [[:space:]]*\S*$/ DefaultModule   $speechProvider/" /etc/speech-dispatcher/speechd.conf |& log
sudo cp /etc/fenrirscreenreader/punctuation/${locale::2}.conf /etc/fenrirscreenreader/punctuation/default.conf |& log
# set the system language
sudo localectl set-locale ${locale}

# Write to the firstboot flag file so that this script will be skipped when first-boot runs again after the system is rebooted
test -f ${HOME}/.firstboot && echo "set" > ${HOME}/.firstboot

# Language settings require reboot to fully take effect if the language is not American English.
if [[ "$language" != "American English" || "${LANG::5}" != "en_US" ]]; then
	gettext -e 'Configuring the computer in your selected language...\n'
	read -t 3 continue
	sudo reboot
fi
