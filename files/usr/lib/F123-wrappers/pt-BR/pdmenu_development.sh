#!/bin/bash
# F123 menu
#
# Copyright 2018, F123 Consulting, <information@f123.org>
# Copyright 2018, Kyle, <kyle@free2.ml>
# Copyright 2018, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--

# Remember to make this script executable so pdmenu can access it.
# Remember for subshells and variables that need to be sent instead of used put a backslash before the dollar sign
export TEXTDOMAIN=pdmenurc
export TEXTDOMAINDIR=/usr/share/locale
. gettext.sh

cat << EOF
title:$(gettext "Welcome to F123Light")

# Define the main menu.
menu:main:$(gettext "F123 Light Main Menu"):$(gettext "Use the up and down arrow keys to select your choice and press the 'Enter' key to activate it.")
	show:$(gettext "_Games Menu (G)")..::games
	show:$(gettext "_Internet Menu (I)")..:$(gettext "Browser, e-mail and chat applications"):internet
	show:$(gettext "_Media Menu (M)")..:$(gettext "Book reading, music and video applications"):media
	show:$(gettext "_Office Menu (O)")..:$(gettext "text, calendar and spreadsheet applications"):office
	exec:$(gettext "_File Manager (F)"):$(gettext "Copy, move and delete files"):clear;command dragonfm
	group:$(gettext "Manage External _Drives (D)")
		exec::makemenu: \
		echo "menu:external:$(gettext "External"):$(gettext "Select Drive")"; \
		rmdir /media/* &> /dev/null; \
		c=0; \
		for i in \$(find /media -maxdepth 1 ! -path /media -type d 2> /dev/null) ; do \
			((c++)); \
			j="\${i/\/media\//}"; \
			echo "exec:_\$j::dragonfm '\$i'"; \
			echo "exec:$(gettext "Safely remove") _\$j::umount '\$i' || sudo umount '\$i'"; \
		done; \
		[[ \$c -gt 0 ]] || echo "exec:\$(gettext "No external drives found")::clear"; \
		echo "exit:$(gettext "Main Menu")..";
		show:::external
		remove:::external
	endgroup
$([[ -r ~/.config/F123/menu ]] && echo "show:$(gettext "Custom Accessories Menu (_U)")..:$(gettext "User Defined Applications"):custom_accessories")
	nop:$(gettext "Search")
    exec:$(gettext "Search This _Computer (C)"):edit,pause:command recoll -t ~Search for what? :~
    exec:$(gettext "Search the _Web (W)"):edit,pause:command ${preferences[searchEngine]} ~Search for what? :~
	nop
	show:$(gettext "_Settings Menu (S)")..:$(gettext "Configure this computer"):settings
	show:$(gettext "_Help Menu (H)")..:$(gettext "Get Help with F123Light"):help
	show:$(gettext "_Turn Off or Restart Computer (T)")..::power
	nop
	exit:$(gettext "E_xit to Command Line")

# Submenu for games.
menu:games:$(gettext "Games")):
	exec:$(gettext "_Adventure (A)"):pause:clear;command adventure
	exec:$(gettext "_Arithmetic Challenge! (A)"):pause:clear;command arithmetic
	#exec:$(gettext "_Air Traffic Controler (Not screen reader friendly) (A)")::clear;command atc
	#exec:$(gettext "_Backgammon (Not screen reader friendly) (B)")::clear;command backgammon
	exec:$(gettext "_Battlestar (B)")::clear;command battlestar
	#exec:$(gettext "_Boggle (Not screen reader friendly) (B)")::clear;command boggle
	#exec:$(gettext "_Canfield (Not screen reader friendly) (C)")::clear;command canfield
	#exec:$(gettext "_Cribbage (Not screen reader friendly) (C)")::clear;command cribbage
	exec:$(gettext "_Go Fish (G)"):pause:clear;command go-fish
	exec:$(gettext "_Gomoku (G)")::clear;command gomoku
	exec:$(gettext "_Hangman (H)")::clear;command hangman
    group:$(gettext "Horseshoes (_H)")
        exec:::clear
        exec:::/usr/lib/F123-wrappers/tips.sh horseshoes
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-ignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
        exec:::command startx /usr/lib/F123-wrappers/x-self-voiced horseshoes
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-unignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    endgroup
	#exec:$(gettext "_Hunt (Not screen reader friendly) (H)")::clear;command hunt
	exec:$(gettext "Legends of _Kallisti (K)")::clear;command /usr/lib/F123-wrappers/mud-loader.sh https://gitlab.com/hjozwiak/tintin-kallisti-pack.git
	exec:$(gettext "_Mille Bornes (M)")::clear;command mille
	exec:$(gettext "_Number (N)")::clear;command number
	exec:$(gettext "_Phantasia (P)")::clear;command phantasia
	exec:$(gettext "_Phase of the Moon (P)"):pause:clear;command pom
	exec:$(gettext "_Primes (P)")::clear;command primes
	#exec:$(gettext "_Robots (Not screen reader friendly) (R)")::clear;command robots
	exec:$(gettext "_Sail (S)")::clear;command sail
	#exec:$(gettext "_Snake (Not screen reader friendly) (S)")::clear;command snake
	#exec:$(gettext "_Tetris (Not screen reader friendly) (T)")::clear;command tetris-bsd
	exec:$(gettext "_Trek (T)")::clear;command trek
	group:$(gettext "_Tux Math (T)")
        exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh tuxmath
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-ignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
        exec:::command startx /usr/lib/F123-wrappers/x-self-voiced tuxmath --tts
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-unignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    endgroup
	group:$(gettext "_Tux Type (T)")
        exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh tuxtype
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-ignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
        exec:::command startx /usr/lib/F123-wrappers/x-self-voiced tuxtype --tts
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-unignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    endgroup
	#exec:$(gettext "_Worm (Not screen reader friendly) (W)")::clear;command worm
	exec:$(gettext "_Wumpus (W)")::clear;command wump
	nop
	exit:$(gettext "_Main Menu (M)")..

# submenu for internet applications.
menu:internet:$(gettext "Internet"):$(gettext "Internet programs")
	group:$(gettext "E-_mail (M)")
        $(/usr/lib/F123-wrappers/mail-launcher.sh)
    endgroup
	nop:$(gettext "Web Browsers")
	group:$(gettext "_Basic Web Browser (W3M) (B)")
		exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh w3m
		exec:::command w3m
    endgroup
	group:$(gettext "_Full Web Browser (Firefox) (F)")
        exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh firefox
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-ignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
        exec:::command startx /usr/lib/F123-wrappers/xlauncher firefox
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-unignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    endgroup
	nop:$(gettext "Communication")
	group:$(gettext "Telegram (T)")
		exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh telegram-cli
		exec:::command telegram-cli
    endgroup
	group:$(gettext "_Text Chat (Pidgin) (T)")
        exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh pidgin
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-ignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
        exec:::command startx /usr/lib/F123-wrappers/xlauncher pidgin
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-unignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    endgroup
	show:$(gettext "Voice Chat (Mumble) (_V)")::mumble
	nop
	exit:$(gettext "_Main Menu (M)")..

menu:mumble:$(gettext "Voice Chat (Mumble)"):$(gettext "Voice Chat (Mumble)")
	exec:$(gettext "Add Server (_A)")::/usr/lib/F123-wrappers/mumble-manager-add-server.sh
	exec:$(gettext "Remove Server (_R)")::/usr/lib/F123-wrappers/mumble-manager-remove-server.sh
	group:$(gettext "Connect (_C)")
		exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh barnard
		exec:::/usr/lib/F123-wrappers/mumble-manager.sh
		exec:::reset
    endgroup
	nop
	exit:$(gettext "Internet Menu (_M)")..

menu:media:$(gettext "Media"):$(gettext "Multi-media applications")
	group:$(gettext "CD _Audio Ripper (A)")
		exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh ripit
		exec:::command ripit
    endgroup
	group:$(gettext "_Music Player (M)")
		exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh cmus
		exec:::command cmus
    endgroup
	group:$(gettext "Stringed _Instrument Tuner (I)")
		exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh bashtuner
		exec:::command bashtuner
    endgroup
	group:$(gettext "_Pandora Internet Radio (P)")
        exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh pianobar
        exec:::test -d "${XDG_CONFIG_HOME:-$HOME/.config}/pianobar" || command /usr/lib/F123-wrappers/configure-pianobar.sh
        exec:::command pianobar
    endgroup
	group:$(gettext "Youtube (_Audio Only) (A)")
		exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh youtube-viewer
		exec:::command youtube-viewer -novideo
    endgroup
	group:$(gettext "Youtube (Full _Video) (V)")
        exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh youtube-viewer
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-ignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
        exec:::command startx /usr/lib/F123-wrappers/xlauncher lxterminal -e youtube-viewer
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-unignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    endgroup
	nop:$(gettext "Book Readers")
	group:$(gettext "_Book Reader (B)")
		exec::makemenu: \
		echo "menu:books:$(gettext "Books"):$(gettext "Select book to read")"; \
		find \$HOME -type f \( -iname "*.epub" -o -iname "*.pdf" \) -print0 |\
		 while read -d \$'\0' i ; do \
			j="\$(basename "\$i")"; \
			echo "exec:_\${j%%.*}::/usr/lib/F123-wrappers/bookreader.sh '\$i'"; \
		done; \
		echo nop; \
		echo "exit:$(gettext "Media Menu").."
		show:::books
		remove:::books
	endgroup
	nop
	exit:$(gettext "_Main Menu (M)")..

menu:office:$(gettext "Office"):$(gettext "Word processing, calendar, etc")
	exec:$(gettext "_Month Calendar (M)"):pause:clear;command ncal
	exec:$(gettext "_Year Calendar (Y)"):pause:clear;command ncal -y
	group:$(gettext "_Spreadsheet (S)")
		exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh sc-im
		exec:::command sc-im
    endgroup
	group:$(gettext "_Text Editor (T)")
		exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh ne
		exec:::command ne --macro clear
    endgroup
	group:$(gettext "_Word Processor (W)")
		exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh wordgrinder
		exec:::command wordgrinder
    endgroup
    nop:$(gettext "OCR")
    group:$(gettext "Lios OCR (_L)")
        exec:::clear
        exec:::/usr/lib/F123-wrappers/tips.sh localc
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-ignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
        exec:::command startx /usr/lib/F123-wrappers/xlauncher lios
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-unignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    endgroup
	nop:$(gettext "Office Suite")
	group:$(gettext "_Spreadsheet (S)")
        exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh localc
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-ignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
        exec:::command startx /usr/lib/F123-wrappers/xlauncher localc
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-unignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    endgroup
	group:$(gettext "_Word Processor (W)")
        exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh lowriter
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-ignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
        exec:::command startx /usr/lib/F123-wrappers/xlauncher lowriter
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-unignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    endgroup
	group:$(gettext "Libre _Office (All Applications) (O)")
        exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh $(shuf -n1 -e localc lowriter)
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-ignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
        exec:::command startx /usr/lib/F123-wrappers/xlauncher soffice
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-unignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    endgroup
	nop
	exit:$(gettext "_Main Menu (M)")..

# submenu for configuring the computer.
menu:settings:$(gettext "Settings"):$(gettext "System configuration")
    show:$(gettext "System Backup Menu (_Y)")..::backup
	exec:$(gettext "Check for System _Updates (U)"):pause:clear;/usr/bin/update-f123light
	exec:$(gettext "_Change Passwords (C)")::clear;/usr/lib/F123-wrappers/configure-passwords.sh
	exec:$(gettext "E-_mail Configuration (M)")::clear;command configure-email
	exec:$(gettext "Securit_y Configuration (Y)")::clear;/usr/lib/F123-wrappers/configure-security.sh
	exec:$(gettext "Configure fast _language switching (L)")::clear;/usr/lib/F123-wrappers/language-chooser.sh
	exec:$(gettext "Change System S_peech (P)")::clear;/usr/lib/F123-wrappers/configure-speech.sh
	group:$(gettext "_Bluetooth manager (B)")
        exec:::clear
		exec:::/usr/lib/F123-wrappers/tips.sh blueman
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-ignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
        exec:::command startx /usr/lib/F123-wrappers/blueman-launcher
        exec:::python /usr/share/fenrirscreenreader/tools/fenrir-unignore-screen &> /dev/null
        exec:::echo -n "setting set screen#suspendingScreen=\$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    endgroup
	exec:$(gettext "Configure _Wifi (W)")::clear;sudo configure-wifi
	nop
	exit:$(gettext "_Main Menu (M)")..

menu:backup:$(gettext "Backup or restore your computer"):$(gettext "Backup or restore your computer")
    exec:$(gettext "Backup your data and settings (_B)")::/usr/lib/F123-wrappers/backup-manager.sh
    exec:$(gettext "Restore your data and settings (_R)")::/usr/lib/F123-wrappers/restore-manager.sh
    exec:$(gettext "Full system backup (_F)")::/usr/lib/F123-wrappers/system-backup.sh
    nop
    exit:$(gettext "Settings Menu (_S)")..
 
menu:help:$(gettext "Get Help with F123 Light"):$(gettext "Get Help with F123Light")
	exec:$(gettext "_Get Help (G)"):pause:command echo -e "For help please subscribe to the F123 visual email list\nhttps://groups.io/g/F123-Visual-English"
    exec:$(gettext "Access text chat to request help (_C)")::command irssi --home /etc/F123-Config/irssi
    exec:$(gettext "Request remote assistance with my computer (_R)"):pause:command sudo cfh
	nop
	exec:$(gettext "About F123Light (_A)"):pause:/usr/lib/F123-wrappers/about.sh
	nop
	exit:$(gettext "_Main Menu")..

menu:power:$(gettext "Turn off or Restart Computer"):$(gettext "Shutdown or restart your computer")
	exec:$(gettext "_Lock (L)")::vlock -a
	exec:$(gettext "Turn _Off (O)")::poweroff &> /dev/null || sudo poweroff
	exec:$(gettext "_Restart (R)")::reboot &> /dev/null || sudo reboot
	nop
	exit:$(gettext "_Main Menu (M)")..

$(if [[ -r ~/.config/F123/menu ]]; then
    echo "menu:custom_accessories:$(gettext "User Defined Applications"):$(gettext "User Defined Applications")"
    cat ~/.config/F123/menu
    echo "nop"
    echo "exit:$(gettext "Main Menu (_M)").."
fi)
EOF
