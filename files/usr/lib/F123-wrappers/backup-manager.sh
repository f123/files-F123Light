#!/bin/bash
# backup-manager.sh
# Description:
#
# Copyright 2019, F123 Consulting, <information@f123.org>
# Copyright 2019, Storm Dragon, <storm_dragon@linux-a11y.org>
#
# This is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this package; see the file COPYING.  If not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
#--code--
 
# Load functions and reusable code:
if [[ -d /usr/lib/F123-includes/ ]]; then
	for i in /usr/lib/F123-includes/*.sh ; do
		source $i
	done
fi

# the gettext essentials
export TEXTDOMAIN=backup-manager
export TEXTDOMAINDIR=/usr/share/locale
source gettext.sh

# Log writing function
log() {
    # Usage: command | log for just stdout.
	# Or command |& log for stderr and stdout.
    while read -r line ; do
        echo "$line" | sudo tee -a "$logFile" &> /dev/null
    done
}
 
# Log file name is /var/log/scriptname
logFile="/var/log/${0##*/}"
# Clear previous logs
echo -n | sudo tee "$logFile" &> /dev/null

# See if we can find a previous backup.
destination="$(find /media -type d -name "storeBackup" -print -quit)"

# If destination is set, prompt to make sure the backup should be done here.
if [[ -n "$destination" ]]; then
	echo "Previous backup found at $destination" | log
	[[ "$(yesno "$(gettext "A previous backup exists. Would you like to backup everything to ") $destination?")" != "Yes" ]] && { unset destination; echo "Destination cleared per user request." | log; }
fi

# If destination is empty there is no previous backup, so prompt for a new one.
if [[ -z "$destination" ]]; then
	echo "No previous backup found. Generating a list of possible backup drives from /media" | log
	declare -a driveList
for i in $(find /media -maxdepth 1 ! -path /media -type d 2> /dev/null) ; do
		echo "$i" | log
		driveList+=($i $i)
	done
	# Present list of destinations to the user.
	destination="$(menulist ${driveList[@]})"
fi

# If destination is still empty exit
if [[ -z "$destination" ]]; then
	echo "No drive selected, user canceled." | log
	exit 0
fi

msgbox "$(gettext "Starting backup. This could take a long time..")"

sudo storeBackup.pl -f /etc/F123-Config/storeBackup.conf --backupDir "$destination"

exit 0
